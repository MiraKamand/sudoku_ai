export const SET_VALUE = 'SET_VALUE'
export const SELECT_CELL = 'SELECT_CELL'
export const SOLVE_PUZZLE = 'SOLVE_PUZZLE'
export const CLEAR_PUZZLES = 'CLEAR_PUZZLES'
export const EASY_PUZZLES = 'EASY_PUZZLES'
export const MEDIUM_PUZZLES = 'MEDIUM_PUZZLES'
export const HARD_PUZZLES = 'HARD_PUZZLES'
export const SOLVE_BACKTRACKING = 'SOLVE_BACKTRACKING'
/**
 * Creates action to set cell to a value.
 * @param x x index of value being set. 
 * @param y y index of value being set.
 * @param value value to set the cell to.
 */
export function setValue(x, y, value) {
    return {
        type: SET_VALUE,
        payload: {
            x,
            y,
            value,
        }
    }
}

/**
 * Creates action to denote a cell was selected.
 * @param x x index of value being selected. 
 * @param y y index of value being selected.
 */
export function selectCell(x, y) {
    return {
        type: SELECT_CELL,
        payload: {
            x,
            y,
        }
    }
}

/**
 * Creates action to sovle the input puzzle. 
 */
export function solvePuzzle() {
    return {
        type: SOLVE_PUZZLE
    }   
}

export function solvePuzzleBackTracking() {
    return {
        type: SOLVE_BACKTRACKING
    }   
}

/**
 * Creates action to clear the input and output puzzles.
 */
export function clearPuzzles() {
    return {
        type: CLEAR_PUZZLES
    }   
}

export function easyPuzzle() {
    return {
        type: EASY_PUZZLES
    }
}

export function mediumPuzzle() {
    return {
        type: MEDIUM_PUZZLES
    }
}

export function hardPuzzle() {
    return {
        type: HARD_PUZZLES
    }
}